#ifndef _PARTICLE_H
#define _PARTICLE_H

#include "animatable.h"

#include <QTime>

#include <vtkSmartPointer.h>    // Required for smart pointer internal ivars.

// Forward VTK class declarations
class vtkActor;
class vtkRenderer;

class Particle : public Animatable
{
    Q_OBJECT

public:
    // Constructor/Destructor
    Particle(const double* start, int startID, const double* end, int endID, vtkRenderer* renderer);
    ~Particle();

    // Getters/Setters
    double* getStart();
    const double* getStart() const;
    int getStartID();
    double* getEnd();
    const double* getEnd() const;
    int getEndID();
    QList< vtkSmartPointer<vtkActor> > getActors();

    void setStart(const double* start);
    void setEnd(const double* end);
    void setValue(double value);
    void setNumOfFrames(int lastFrame);
    void setNumOfActors(int num);
    void setColor(const double* color);

    void visibilityOff();
    void visibilityOn();

private:
    QTime m_time;
    // VTK variables
    QList< vtkSmartPointer<vtkActor> > m_actors;
    double m_startPos[3];
    int m_startID;
    double m_endPos[3];
    int m_endID;
    double m_pos[3];
    double m_dir[3];
    int m_lastFrame;
    double m_sinDir[3];
    double m_value;
    double m_color[3];
    int m_numOfActors;
    int m_maxNumOfActors;

public slots:
    void update(int frame) Q_DECL_OVERRIDE;
signals:
    void endParticleFrame();
};

#endif() /* _ PARTICLE_H */
