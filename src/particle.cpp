#include "particle.h"

#include <QDebug>

#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkMath.h>

#define VTK_CREATE(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

#define VTK_INIT(type, name) name = vtkSmartPointer<type>::New()

Particle::Particle(const double* start, int startID, const double* end, int endID, vtkRenderer* renderer) : Animatable(),
                                                                                                            m_numOfActors(5),
                                                                                                            m_maxNumOfActors(5)
{
    //if (vtkMath::Random() > 0.5)
    //{
        m_startID = startID;
        m_endID = endID;
        for (int i = 0; i < 3; i++)
        {
            m_startPos[i] = start[i];
            m_pos[i] = start[i];
            m_endPos[i] = end[i];
        }
    //}
   /* else
    {
        m_startID = endID;
        m_endID = startID;
        for (int i = 0; i < 3; i++)
        {
            m_startPos[i] = end[i];
            m_pos[i] = end[i];
            m_endPos[i] = start[i];
        }
    }
*/
    vtkMath::Subtract(m_endPos, m_startPos, m_dir);

    double rand[3];
    rand[0] = vtkMath::Random();
    rand[1] = vtkMath::Random();
    rand[2] = vtkMath::Random();

    vtkMath::Cross(m_dir, rand, m_sinDir);
    vtkMath::Normalize(m_sinDir);

    for (int i = 0; i < m_maxNumOfActors; i++)
    {
        VTK_CREATE(vtkSphereSource, sphereSource);
        sphereSource->Update();

        VTK_CREATE(vtkPolyDataMapper, sphereMapper);
        sphereMapper->SetInputConnection(sphereSource->GetOutputPort());

        VTK_CREATE(vtkActor, actor);
        actor->SetMapper(sphereMapper);
        actor->SetPosition(m_pos);
        actor->GetProperty()->BackfaceCullingOn();
        actor->VisibilityOff();

        actor->SetScale(2.4 - i * 0.4);

        renderer->AddActor(actor);

        m_actors.push_back(actor);
    }
}

Particle::~Particle()
{}

double* Particle::getStart()
{
    return m_startPos;
}

const double* Particle::getStart() const
{
    return m_startPos;
}

int Particle::getStartID()
{
    return m_startID;
}

double* Particle::getEnd()
{
    return m_endPos;
}

const double* Particle::getEnd() const
{
    return m_endPos;
}

int Particle::getEndID()
{
    return m_endID;

}

QList< vtkSmartPointer<vtkActor> > Particle::getActors()
{
    return m_actors;
}

void Particle::setStart(const double* start)
{
    for (int i = 0; i < 3; i++)
    {
        m_startPos[i] = start[i];
    }
}

void Particle::setEnd(const double* end)
{
    for (int i = 0; i < 3; i++)
    {
        m_endPos[i] = end[i];
    }
}

void Particle::setValue(double value)
{
    m_value = value;
}

void Particle::setNumOfFrames(int lastFrame)
{
    m_lastFrame = lastFrame;
}

void Particle::setColor(const double* color)
{
    for (int i = 0; i < 3; i++)
    {
        m_color[i] = color[i];
    }
    for (int i = 0; i < 5; i++)
    {
        m_actors[i]->GetProperty()->SetColor(m_color);
    }
}

void Particle::setNumOfActors(int num)
{
    if (num > m_maxNumOfActors)
    {
        qDebug() << "Cannot set more than" << m_maxNumOfActors << "actors";
        return;
    }

    m_numOfActors = num;
    double step = 1.4 / (num - 1);
    int i;
    for (i = 0; i < num; i++)
    {
        m_actors[i]->SetScale(2.6 - i * step);
    }
    for (; i < m_maxNumOfActors; i++)
    {
        m_actors[i]->VisibilityOff();
    }
}

void Particle::visibilityOff()
{
    for (int i = 0; i < m_maxNumOfActors; i++)
    {
        m_actors[i]->VisibilityOff();
    }
}

void Particle::visibilityOn()
{
    for (int i = 0; i < m_maxNumOfActors; i++)
    {
        m_actors[i]->VisibilityOn();
    }
}

void Particle::update(int frame)
{
    //cout << "Update frame: " << frame <<"/" << m_lastFrame << endl;
    //qDebug() << "Particle " << "from ->" << m_startID << "to" << m_endID << "frame" << frame << "/" << m_finalFrame;
    //qDebug() << "Before" << m_pos[0] << m_pos[1] << m_pos[2];
    //qDebug() << "Start" << m_startPos[0] << m_startPos[1] << m_startPos[2];
    //qDebug() << "End" << m_endPos[0] << m_endPos[1] << m_endPos[2];
    
    int  w = m_lastFrame/50;

    if (m_lastFrame < 50) w = 1;
    else if (m_lastFrame < 100) w = 2;
    else if (m_lastFrame < 150) w = 3;
    else if (m_lastFrame < 100) w = 4;
    else if (m_lastFrame < 250) w = 5;
    else w = 6;
    int relevantFrame;
    double tempPos[3], cosPos[3];
    for (int i = 0; i < m_numOfActors; i++)
    {
        relevantFrame = frame - i;
        if ((relevantFrame >= 0) && (relevantFrame < m_lastFrame))
        {
            //qDebug() << "Temp" << tempPos[0] << tempPos[1] << tempPos[2];
            double fraction = (double)relevantFrame / (m_lastFrame - 1);
            for (int j = 0; j < 3; j++)
            {
                tempPos[j] = m_dir[j] * fraction;
                //cosPos[j] = 5 * m_value * m_sinDir[j] * cos(relevantFrame * vtkMath::Pi() / 4);
            }
            //qDebug() << "Temp" << tempPos[0] << tempPos[1] << tempPos[2];
            //vtkMath::Add(tempPos, cosPos, tempPos);
            vtkMath::Add(m_startPos, tempPos, m_pos);
            //qDebug() << "After" << m_pos[0] << m_pos[1] << m_pos[2];

            //m_actors[j]->SetScale(1.5 + m_value * cos(7 * vtkMath::Pi() * fraction));
            m_actors[i]->SetPosition(m_pos);
            m_actors[i]->VisibilityOn();
        }
        else
        {
            m_actors[i]->VisibilityOff();
        }
    }

    emit endParticleFrame();
}