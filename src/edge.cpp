#include "edge.h"

Edge::Edge(Node& from, int fromID, Node& to, int toID, vtkSmartPointer<vtkActor> actor) : m_from(from),
                                                                                        m_fromID(fromID),
                                                                                        m_to(to),
                                                                                        m_toID(toID),
                                                                                        m_actor(actor)
{}

Edge::~Edge()
{}

Node& Edge::getFrom()
{
    return m_from;
}

int Edge::getFromID()
{
    return m_fromID;
}

int Edge::getToID()
{
    return m_toID;
}

Node& Edge::getTo()
{
    return m_to;
}

vtkSmartPointer<vtkActor> Edge::getActor()
{
    return m_actor;
}

void Edge::setFrom(Node& node)
{
    m_from = node;
}

void Edge::setTo(Node& node)
{
    m_to = node;
}

void Edge::update()
{
}