﻿// QT includes
#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    // QT Stuff
    QApplication app(argc, argv);
    
    MainWindow win;
    win.show();
    
    return app.exec();
}
