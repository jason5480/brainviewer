#include "brain.h"

//#include <QMetaObject>
#include <QMetaProperty>
#include <QString>
#include <QVTKInteractor.h>
#include <QDirIterator>
#include <QStandardItemmodel>
#include <QDebug>

#include <vtkOBJReader.h>
#include <vtkCylinderSource.h>
#include <vtkSphereSource.h>
#include <vtkAppendPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkReverseSense.h>
#include <vtkRenderer.h>
#include <vtkNamedColors.h>
#include <vtkColorTransferFunction.h>
#include <vtkMath.h>
#include <vtkTransform.h>
#include <vtkOctreePointLocator.h>
#include <vtkBalloonWidget.h>
#include <vtkBalloonRepresentation.h>

#define VTK_CREATE(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

#define VTK_INIT(type, name) name = vtkSmartPointer<type>::New()

Brain::Brain() : Animatable(),
                 m_modelNodes(0),
                 m_prevActReg(),
                 m_particleFrame(0),
                 m_particlesLastFrame(0),
                 m_particleSpeed(80),
                 m_timeWindowLastFrame(0),
                 m_particlesUpdated(0)
{
    // Auxiliary Variables
    VTK_INIT(vtkOctreePointLocator, m_octreeRegions);
    VTK_INIT(vtkOctreePointLocator, m_octreeEEG);
    VTK_INIT(vtkNamedColors, m_namedColors);
    VTK_INIT(vtkColorTransferFunction, m_colorTransferFunction);
    m_colorTransferFunction->AddRGBPoint(0.5, 0.0, 0.0, 1.0);
    m_colorTransferFunction->AddRGBPoint(1.0, 1.0, 0.0, 0.0);
    m_timer.setInterval(40);
    m_timer.start();
}

Brain::~Brain()
{
    if (m_modelNodes) delete m_modelNodes;
    for (int i = 0; i < m_modelAlphaEdges.count(); i++)
    {
        delete m_modelAlphaEdges[i];
    }
    for (int i = 0; i < m_modelThetaEdges.count(); i++)
    {
        delete m_modelThetaEdges[i];
    }

    for (int i = 0; i < m_particles.count(); i++)
    {
        delete m_particles[i];
    }
    m_timer.stop();
}

Graph& Brain::getEEGGraph()
{
    return m_graphEEG;
}

Graph& Brain::getRegionsGraph()
{
    return m_graphRegions;
}

vtkSmartPointer<vtkActor> Brain::getActor()
{
    return m_actor;
}

void Brain::setupBrainActor(QString brainPath, bool visibility, double opacity, vtkRenderer* renderer)
{
    VTK_CREATE(vtkOBJReader, brainReader);
    VTK_CREATE(vtkAppendPolyData, brainPolyData);

    brainReader->SetFileName(brainPath.toStdString().c_str());
    brainReader->Update();

    VTK_CREATE(vtkPolyDataMapper, brainPolyDataMapper);
    brainPolyDataMapper->SetInputConnection(brainReader->GetOutputPort());

    VTK_INIT(vtkActor, m_actor);
    m_actor->SetMapper(brainPolyDataMapper);
    m_actor->GetProperty()->BackfaceCullingOn();
    m_actor->SetScale(1.02);
    m_actor->GetProperty()->SetOpacity(opacity);
    //m_actor->GetProperty()->SetColor(m_namedColors->GetColor3d("SteelBlue").GetData());
    m_actor->GetProperty()->SetColor(m_namedColors->GetColor3d("White").GetData());
    m_actor->SetVisibility(visibility);

    renderer->AddActor(m_actor);
}

void Brain::setupBrainRegionsGraph(QString resourcesDirPath, bool visibility, double opacity, vtkRenderer* renderer)
{
    VTK_CREATE(vtkPoints, centerePoints);
    VTK_CREATE(vtkPolyData, regionsCenPolyData);

    double* defaultColor = m_namedColors->GetColor3d("SteelBlue").GetData();
    double activeColor[3];

    QDirIterator it(resourcesDirPath, QStringList() << "*.obj",
        QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext())
    {
        VTK_CREATE(vtkOBJReader, brainRegionReader);
        brainRegionReader->SetFileName(it.next().toStdString().c_str());
        brainRegionReader->Update();

        VTK_CREATE(vtkPolyDataMapper, brainRegionMapper);
        brainRegionMapper->SetInputConnection(brainRegionReader->GetOutputPort());
        brainRegionMapper->Update();

        // Actors
        VTK_CREATE(vtkActor, brainRegionNodeActor);
        brainRegionNodeActor->SetMapper(brainRegionMapper);
        brainRegionNodeActor->GetProperty()->SetColor(defaultColor);
        brainRegionNodeActor->GetProperty()->BackfaceCullingOn();
        brainRegionNodeActor->GetProperty()->SetOpacity(opacity);
        brainRegionNodeActor->SetVisibility(visibility);

        // Add actor to the list
        QString filename = it.fileName();
        QStringList ls = filename.split(QString("."));
        QString label = ls.value(ls.length() - 2);

        vtkIdType iD = m_octreeEEG->FindClosestPoint(brainRegionNodeActor->GetCenter());
        double *nodePos, pos[3];
        nodePos = m_octreeEEG->GetDataSet()->GetPoint(iD);
        vtkMath::Subtract(brainRegionNodeActor->GetCenter(), nodePos, pos);
        for (int k = 0; k < 3; k++) pos[k] /= 1.5;
        vtkMath::Add(nodePos, pos, pos);

        // Create the Node
        Node n(label, pos, brainRegionNodeActor);
        activeColor[0] = vtkMath::Random();
        activeColor[1] = vtkMath::Random();
        activeColor[2] = vtkMath::Random();
        n.setDefaultColor(defaultColor);
        n.setActiveColor(activeColor);
        m_graphRegions.addNode(n);

        m_balloonWidget->AddBalloon(brainRegionNodeActor, label.toStdString().c_str());

        // Add to the renderer
        renderer->AddActor(brainRegionNodeActor);

        // Add center in the points
        centerePoints->InsertNextPoint(brainRegionNodeActor->GetCenter());
    }

    // Create the Octree based on regions
    regionsCenPolyData->SetPoints(centerePoints);

    m_octreeRegions->SetDataSet(regionsCenPolyData);
    m_octreeRegions->BuildLocator();

    // Set Region Edges
    int numn = m_graphRegions.getNodes().size();
    for (int i = 0; i < numn; i++)
    {
        for (int j = 0; j < numn; j++)
        {
            if (i > j)
            {
                VTK_CREATE(vtkCylinderSource, cylinderSource);
                cylinderSource->SetRadius(1);
                cylinderSource->SetResolution(18);
                cylinderSource->Update();

                Node& from = m_graphRegions.getNode(i);
                Node& to = m_graphRegions.getNode(j);
                double pos[3];
                const double* pos1 = from.getPos();
                const double* pos2 = to.getPos();

                vtkMath::Add(pos1, pos2, pos);
                for (int k = 0; k < 3; k++) pos[k] /= 2.;

                double dir[3];
                vtkMath::Subtract(pos1, pos2, dir);

                double scaleY = sqrt(dir[0] * dir[0] +
                    dir[1] * dir[1] +
                    dir[2] * dir[2]);
                vtkMath::Normalize(dir);

                double orientaton[3];
                double axisY[] = { 0, 1, 0 };
                double axis[3];
                double angle;

                vtkMath::Cross(axisY, dir, axis);
                vtkMath::Normalize(axis);
                angle = vtkMath::DegreesFromRadians(acos(vtkMath::Dot(axisY, dir)));

                VTK_CREATE(vtkTransform, transform);
                transform->Identity();
                transform->RotateWXYZ(angle, axis);
                transform->GetOrientation(orientaton);

                VTK_CREATE(vtkPolyDataMapper, cylinderMapper);
                cylinderMapper->SetInputConnection(cylinderSource->GetOutputPort());

                VTK_CREATE(vtkActor, edgeRegionActor);
                edgeRegionActor->SetMapper(cylinderMapper);
                edgeRegionActor->GetProperty()->SetColor(m_namedColors->GetColor3d("Yellow").GetData());
                edgeRegionActor->SetScale(1, scaleY, 1);
                edgeRegionActor->SetOrientation(orientaton);
                edgeRegionActor->SetPosition(pos);
                edgeRegionActor->VisibilityOff();

                // Add to the list
                m_graphRegions.createEdge(from, i, to, j, edgeRegionActor);

                // Create particle
                Particle* p = new Particle(from.getPos(), i, to.getPos(), j, renderer);
                p->setColor(from.getActiveColor());
                int particleLastFrame = vtkMath::Distance2BetweenPoints(from.getPos(), to.getPos()) / m_particleSpeed;
                p->setNumOfFrames(particleLastFrame);
                if (particleLastFrame > m_particlesLastFrame) m_particlesLastFrame = particleLastFrame;
                connect(p, &Particle::endParticleFrame, this, &Brain::checkParticles);
                m_particles.push_back(p);

                // Add to the renderer
                renderer->AddActor(edgeRegionActor);
            }
        }
    }
    //qDebug() << "Tottal particles created:" << m_particles.size();
}

void Brain::setupBalloonWidget(QVTKInteractor* interactor)
{
    VTK_CREATE(vtkBalloonRepresentation, balloonRepresentation);
    balloonRepresentation->SetBalloonLayoutToImageTop();
    VTK_INIT(vtkBalloonWidget, m_balloonWidget);
    m_balloonWidget->SetInteractor(vtkRenderWindowInteractor::SafeDownCast(interactor));
    m_balloonWidget->SetRepresentation(balloonRepresentation);
    m_balloonWidget->EnabledOn();
}

void Brain::setupEEGNodes(QStandardItemModel* modelNodes, vtkRenderer* renderer)
{
    VTK_CREATE(vtkPoints, centerePoints);
    VTK_CREATE(vtkPolyData, eegCenPolyData);

    m_modelNodes = modelNodes;

    int rows = modelNodes->rowCount();
    int cols = modelNodes->columnCount();

    for (int i = 1; i < rows; i++)
    {
        double cen[3];
        cen[0] = modelNodes->item(i, 1)->text().toDouble();
        cen[1] = modelNodes->item(i, 2)->text().toDouble() - 16;
        cen[2] = modelNodes->item(i, 3)->text().toDouble() - 21;

        VTK_CREATE(vtkSphereSource, sphereSource);
        //sphereSource->SetCenter(cen);
        sphereSource->SetRadius(3);
        sphereSource->Update();

        VTK_CREATE(vtkPolyDataMapper, sphereMapper);
        sphereMapper->SetInputConnection(sphereSource->GetOutputPort());

        VTK_CREATE(vtkActor, nodeActor);
        nodeActor->SetMapper(sphereMapper);
        nodeActor->SetPosition(cen);
        nodeActor->GetProperty()->SetColor(m_namedColors->GetColor3d("Yellow").GetData());
        nodeActor->GetProperty()->BackfaceCullingOn();
        nodeActor->SetVisibility(false);

        m_balloonWidget->AddBalloon(nodeActor, modelNodes->item(i, 0)->text().toStdString().c_str());

        m_graphEEG.createNode(modelNodes->item(i, 0)->text(), cen, nodeActor);

        // Add to the renderer
        renderer->AddActor(nodeActor);

        // Add center in the points
        centerePoints->InsertNextPoint(cen);
    }

    // Create the Octree based on regions
    eegCenPolyData->SetPoints(centerePoints);

    m_octreeEEG->SetDataSet(eegCenPolyData);
    m_octreeEEG->BuildLocator();
}

void Brain::setupEEGEdges(QList<QStandardItemModel*>& modelAlphaEdges, QList<QStandardItemModel*>& modelThetaEdges, vtkRenderer* renderer)
{
    m_modelAlphaEdges = modelAlphaEdges;
    m_modelThetaEdges = modelThetaEdges;

    // Create all possible edges and hide them
    int numn = (m_graphEEG.getNodes().size());
    for (int i = 0; i < numn; i++)
    {
        for (int j = 0; j < numn; j++)
        {
            if (i > j)
            {
                VTK_CREATE(vtkCylinderSource, cylinderSource);
                cylinderSource->SetRadius(1);
                cylinderSource->SetResolution(18);
                cylinderSource->Update();

                double pos[3], *pos1, *pos2;
                Node& from = m_graphEEG.getNode(i);
                Node& to = m_graphEEG.getNode(j);
                pos1 = from.getActor()->GetCenter();
                pos2 = to.getActor()->GetCenter();

                vtkMath::Add(pos1, pos2, pos);
                for (int k = 0; k <3; k++) pos[k] /= 2.;

                double dir[3];
                vtkMath::Subtract(pos1, pos2, dir);

                double scaleY = sqrt(dir[0] * dir[0] +
                    dir[1] * dir[1] +
                    dir[2] * dir[2]);
                vtkMath::Normalize(dir);

                double orientaton[3];
                double axisY[] = { 0, 1, 0 };
                double axis[3];
                double angle;

                vtkMath::Cross(axisY, dir, axis);
                vtkMath::Normalize(axis);
                angle = vtkMath::DegreesFromRadians(acos(vtkMath::Dot(axisY, dir)));

                VTK_CREATE(vtkTransform, transform);
                transform->Identity();
                transform->RotateWXYZ(angle, axis);
                transform->GetOrientation(orientaton);

                VTK_CREATE(vtkPolyDataMapper, cylinderMapper);
                cylinderMapper->SetInputConnection(cylinderSource->GetOutputPort());

                VTK_CREATE(vtkActor, edgeActor);
                edgeActor->SetMapper(cylinderMapper);
                edgeActor->GetProperty()->SetColor(m_namedColors->GetColor3d("Red").GetData());
                edgeActor->SetScale(1, scaleY, 1);
                edgeActor->SetOrientation(orientaton);
                edgeActor->SetPosition(pos);
                edgeActor->VisibilityOff();

                m_graphEEG.createEdge(from, i, to, j, edgeActor);

                // Add to the renderer
                renderer->AddActor(edgeActor);
            }
        }

    }
}

void Brain::setNumOfFrames(const int lastFrame)
{
    m_timeWindowLastFrame = lastFrame;
}

void Brain::resetEEGNodes()
{
    QList<Node>& nodesEEG = m_graphEEG.getNodes();
    QList<Node>& nodesRegion = m_graphRegions.getNodes();

    qDebug() << "Nomber of EEG nodes is" << nodesEEG.size();
    qDebug() << "Nomber of Region nodes is" << nodesRegion.size();
    double regionPos[3];
    for (int i = 0; i < nodesEEG.size(); i++)
    {
        regionPos[0] = nodesRegion[i].getPos()[0];
        regionPos[1] = nodesRegion[i].getPos()[1];
        regionPos[2] = nodesRegion[i].getPos()[2];
        nodesEEG[i].getActor()->SetPosition(regionPos);
    }
}

void Brain::resetVisibility(bool actor, bool nodes, bool regions)
{
    for (int i = 0; i < m_graphRegions.getNodes().size(); i++)
    {
        m_graphRegions.getNode(i).getActor()->SetVisibility(regions);
        m_graphRegions.getNode(i).getActor()->GetProperty()->SetColor(m_namedColors->GetColor3d("SteelBlue").GetData());
    }

    for (int i = 0; i < m_graphRegions.getEdges().size(); i++)
        m_graphRegions.getEdge(i).getActor()->SetVisibility(nodes);

    for (int i = 0; i < m_particles.size(); i++)
        m_particles[i]->visibilityOff();

    m_actor->SetVisibility(actor);
}

/**
* Invert the normals of the model that the .obj reader read
* @param reader the current .obj reader
*/
void Brain::invertNormals()
{
    // Inverse Normals
    vtkSmartPointer<vtkFloatArray> pointNormals = vtkFloatArray::SafeDownCast(m_actor->GetMapper()->GetInput()->GetPointData()->GetNormals());

    VTK_CREATE(vtkReverseSense, reverseSense);
    reverseSense->SetInputConnection(m_actor->GetMapper()->GetOutputPort());
    reverseSense->ReverseNormalsOn();
    reverseSense->Update();

    vtkSmartPointer<vtkFloatArray> reversedNormals =
        vtkFloatArray::SafeDownCast(reverseSense->GetOutput()->GetPointData()->GetNormals());

    m_actor->GetMapper()->GetInput()->GetPointData()->SetNormals(reversedNormals);
}

void Brain::initShading()
{
    /*
    // Shader
    // Get source from file
    QFile vertexFile(QString(QApplication::applicationDirPath() + "/../../src/shaders/glassVert.txt"));
    vertexFile.open(QIODevice::ReadOnly);
    QByteArray vertBytes = vertexFile.readAll();
    vertexFile.close();
    QString vertexShaderSource(vertBytes);
    //qDebug() << vertexShaderSource;
    // Create program
    VTK_CREATE(vtkShaderProgram2, program);
    program->SetContext(m_renderWin);
    // Crete shader
    VTK_CREATE(vtkShader2, vertexShader);
    vertexShader->SetType(VTK_SHADER_TYPE_VERTEX);
    vertexShader->SetSourceCode(vertexShaderSource.toStdString().c_str());
    //shader->SetUniformVariables()
    vertexShader->SetContext(program->GetContext());
    // Add shaders to the program
    program->GetShaders()->AddItem(vertexShader);

    QFile fragmentFile(QString(QApplication::applicationDirPath() + "/../../src/shaders/glassFrag.txt"));
    fragmentFile.open(QIODevice::ReadOnly);
    QByteArray fragBytes = fragmentFile.readAll();
    fragmentFile.close();
    QString fragmentShaderSource(fragBytes);
    //qDebug() << fragmentShaderSource;
    // Create shader
    VTK_CREATE(vtkShader2, fragmentShader);
    fragmentShader->SetType(VTK_SHADER_TYPE_FRAGMENT);
    fragmentShader->SetSourceCode(fragmentShaderSource.toStdString().c_str());
    //shader->SetUniformVariables()
    fragmentShader->SetContext(program->GetContext());
    // Add shaders to the program
    program->GetShaders()->AddItem(fragmentShader);

    // Set the OpenGL Property
    vtkSmartPointer<vtkOpenGLProperty> openGLproperty =
    static_cast<vtkOpenGLProperty*>(brainActor->GetProperty());
    openGLproperty->SetPropProgram(program);
    openGLproperty->ShadingOn();
    //*/
}

// Slots
void Brain::update(int frame)
{
    //qDebug() << "Brain update";

    /*if (m_frame == 0) m_actor->VisibilityOn();
    else m_actor->VisibilityOff();*/

    // Set invisible previous nodes and edges
    int numprevn = m_prevNodeReg.size();
    for (int i = 0; i < numprevn; i++)
    {
        //m_graphRegions.getNode(m_prevNodeReg[i]).getActor()->VisibilityOff();
        //m_graphRegions.getNode(m_prevNodeReg[i]).getActor()->GetProperty()->SetColor(m_namedColors->GetColor3d("SteelBlue").GetData());
        m_graphRegions.getNode(m_prevNodeReg[i]).deactivate();
    }
    m_prevNodeReg.clear();

    /*int numpreve = m_prevEdgeReg.size();
    for (int i = 0; i < numpreve; i++)
    {
        m_graphRegions.getEdge(m_prevEdgeReg[i]).getActor()->VisibilityOff();
    }
    m_prevEdgeReg.clear();*/

    int numprevp = m_prevParReg.size();
    for (int i = 0; i < numprevp; i++)
    {
        m_particles[i]->visibilityOff();
        disconnect(this, &Brain::newParticleFrame, m_particles[i], &Particle::update);
    }
    m_prevParReg.clear();

    // Update region edges and nodes based on EEG matricies
    //*
    QList<Edge>& edgesEEG = m_graphEEG.getEdges();
    Node* currentNodeElectrode1 = NULL;
    Node* currentNodeElectrode2 = NULL;
    Node *currentNodeRegion1 = NULL;
    vtkActor *currentNodeRegionActor1 = NULL;
    Node *currentNodeRegion2 = NULL;
    vtkActor *currentNodeRegionActor2 = NULL;
    vtkActor *currentEdgeRegionActor = NULL;
    double* color = NULL;
    double alphaValue, thetaValue;
    vtkIdType node1regionID, node2regionID;

    int numee = edgesEEG.size();
    for (int i = 0; i < numee; i++)
    {
        alphaValue = m_modelAlphaEdges[frame]->item(edgesEEG[i].getFromID(), edgesEEG[i].getToID())->text().toDouble();
        if (alphaValue > 0.5)
        {
            thetaValue = m_modelThetaEdges[frame]->item(edgesEEG[i].getFromID(), edgesEEG[i].getToID())->text().toDouble();

            currentNodeElectrode1 = &edgesEEG[i].getFrom();
            currentNodeElectrode2 = &edgesEEG[i].getTo();

            node1regionID = m_octreeRegions->FindClosestPoint(currentNodeElectrode1->getPos());
            node2regionID = m_octreeRegions->FindClosestPoint(currentNodeElectrode2->getPos());

            if (node1regionID == node2regionID) continue;

            // Get regions and show them
            currentNodeRegion1 = &m_graphRegions.getNode(node1regionID);
            currentNodeRegion2 = &m_graphRegions.getNode(node2regionID);
            //currentNodeRegionActor1 = currentNodeRegion1->getActor();
            //currentNodeRegionActor2 = currentNodeRegion2->getActor();

            currentNodeRegion1->activate();
            currentNodeRegion2->activate();
            //currentNodeRegionActor1->VisibilityOn();
            //currentNodeRegionActor2->VisibilityOn();

            /*color = m_colorTransferFunction->GetColor(alphaValue);
            currentNodeRegionActor1->GetProperty()->SetColor(color[0], color[1], color[2]);
            currentNodeRegionActor2->GetProperty()->SetColor(color[0], color[1], color[2]);*/

            // Find the edge
            /*int numre = m_graphRegions.getEdges().size();
            for (int j = 0; j < numre; j++)
            {
                Edge& currEdge = m_graphRegions.getEdge(j);
                if ((currEdge.getFromID() == node1regionID && currEdge.getToID() == node2regionID) || (currEdge.getFromID() == node2regionID && currEdge.getToID() == node1regionID))
                {
                    currentEdgeRegionActor = currEdge.getActor();
                    m_prevEdgeReg.push_back(j);
                    break;
                }
            }*/
            
            // Find particles and connect them with timer
            int numrp = m_particles.size();
            for (int j = 0; j < numrp; j++)
            {
                Particle* currPar = m_particles[j];
                if ((currPar->getStartID() == node1regionID && currPar->getEndID() == node2regionID) ||
                    (currPar->getStartID() == node2regionID && currPar->getEndID() == node1regionID))
                {
                    //currentEdgeRegionActor = currEdge.getActor();
                    currPar->setValue(alphaValue);
                    //cout << "For theta value " << thetaValue << "I assighned" << (int)((thetaValue / 0.2) + 1) << endl;
                    currPar->setNumOfActors((thetaValue / 0.2) + 1);
                    //currPar->visibilityOn();
                    connect(this, &Brain::newParticleFrame, currPar, &Particle::update);
                    m_prevParReg.push_back(j);
                    break;
                }
                
            }

            //if (currentEdgeRegionActor)
            //{
            //    currentEdgeRegionActor->VisibilityOn();
            //    //currentEdgeRegionActor->GetProperty()->SetColor(color[0], color[1], color[2]);
            //    double scale[3];
            //    currentEdgeRegionActor->GetScale(scale);
            //    value = pow(10, value - 0.7);
            //    scale[0] = value;
            //    scale[2] = value;
            //    currentEdgeRegionActor->SetScale(scale);
            //}
            //else cout << "Failed to match edge" << endl;

            m_prevNodeReg.push_back(node1regionID);
            m_prevNodeReg.push_back(node2regionID);
        }
    }
    m_particlesUpdated = 0;
    connect(&m_timer, &QTimer::timeout, this, &Brain::updateParticles);
    //*/

    // Update Regions Randomly
    /*
    vtkActor *currentActor;
    for (int i = 0; i < m_prevActReg.size(); i++)
    {
        Edge& e = m_graphRegions.getEdge(m_prevActReg[i]);
        e.getActor()->VisibilityOff();

        e.getFrom()->getActor()->VisibilityOff();
        e.getTo()->getActor()->VisibilityOff();
    }
    m_prevActReg.clear();

    int numReg = (int)vtkMath::Random(1, 4);
    int nume = m_graphRegions.getEdges().size();
    for (int i = 0; i < numReg; i++)
    {
        int actReg = (int)vtkMath::Random(0, nume-1);
        Edge& edge = m_graphRegions.getEdge(actReg);
        edge.getFrom()->getActor()->VisibilityOn();
        edge.getTo()->getActor()->VisibilityOn();
        currentActor = edge.getActor();
        currentActor->VisibilityOn();
        currentActor->GetProperty()->SetColor(m_namedColors->GetColor3d("Yellow").GetData());

        m_prevActReg.push_back(actReg);
    }
    //*/
}

void Brain::updateParticles()
{
    if (m_particleFrame < (m_particlesLastFrame + 4))
    {
        //qDebug() << "Brain emit newFrame" << m_frame;
        emit newParticleFrame(m_particleFrame++);
    }
    else
    {
        m_particleFrame = 0;
        disconnect(&m_timer, &QTimer::timeout, this, &Brain::updateParticles);
        emit endTimeWindowFrame();
        //qDebug() << "Brain STOP emit newFrame";
    }
}

void Brain::checkParticles()
{
    m_particlesUpdated++;
    if (m_particlesUpdated == m_prevParReg.size())
    {
        //qDebug() << "All" << m_particlesUpdated << "particles updated";
        m_particlesUpdated = 0;
        emit readyForRendering();
    }
}