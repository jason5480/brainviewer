#include "graph.h"

Graph::Graph() : m_nodes(),
                 m_edges()
{}

Graph::~Graph()
{}

QList<Node>& Graph::getNodes()
{
    return m_nodes;
}

Node& Graph::getNode(int i)
{
    return m_nodes[i];
}

QList<Edge>& Graph::getEdges()
{
    return m_edges;
}

Edge& Graph::getEdge(int i)
{
    return m_edges[i];
}

void Graph::setNode(int i, const double* pos, vtkSmartPointer<vtkActor> actor)
{
    if (i >= m_nodes.size())
    {
        cout << i << " too large" << endl;
        return;
    }
    m_nodes[i].setPos(pos);
    m_nodes[i].setActor(actor);
}

void Graph::createNode(QString label, const double* pos, vtkSmartPointer<vtkActor> actor)
{
    m_nodes.push_back(Node(label, pos, actor));
}

void Graph::createEdge(Node& from, int fromID, Node& to, int toID, vtkSmartPointer<vtkActor> actor)
{
    m_edges.push_back(Edge(from, fromID, to, toID, actor));
}

void Graph::addNode(Node& node)
{
    m_nodes.push_back(node);
}

void Graph::addEdge(Edge& edge)
{
    m_edges.push_back(edge);
}