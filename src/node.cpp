#include "node.h"

#include <vtkActor.h>
#include <vtkProperty.h>

Node::Node() : m_label(),
               m_actor()
{}

Node::Node(QString label, const double* pos, vtkSmartPointer<vtkActor> actor)
{
    setLabel(label);
    setPos(pos);
    setActor(actor);
}

Node::~Node()
{}

const QString& Node::getLabel() const
{
    return m_label;
}

const double* Node::getPos() const
{
    return m_pos;
}

const double* Node::getActiveColor()
{
    return m_activeColor;
}

vtkSmartPointer<vtkActor> Node::getActor()
{
    return m_actor;
}

void Node::setLabel(const QString& label)
{
    m_label = label;
}

void Node::setPos(const double* pos)
{
    for (int i = 0; i < 3; i++)
    {
        m_pos[i] = pos[i];
    }
}

void Node::setActor(vtkSmartPointer<vtkActor> actor)
{
    m_actor = actor;
}

void Node::setDefaultColor(const double* color)
{
    for (int i = 0; i < 3; i++)
    {
        m_defaultColor[i] = color[i];
    }
}

void Node::setActiveColor(const double* color)
{
    for (int i = 0; i < 3; i++)
    {
        m_activeColor[i] = color[i];
    }
}

void Node::activate()
{
    m_actor->GetProperty()->SetColor(m_activeColor);
}

void Node::deactivate()
{
    m_actor->GetProperty()->SetColor(m_defaultColor);
}