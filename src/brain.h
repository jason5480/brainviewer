#ifndef _BRAIN_H
#define _BRAIN_H

#include "animatable.h"
#include "particle.h"
#include "graph.h"

#include <QList>
#include <QStandardItemmodel>
#include <QTimer>

#include <vtkSmartPointer.h>    // Required for smart pointer internal ivars.

// Forward Qt class declarations
class QString;
class QVTKInteractor;

// Forward VTK class declarations
class vtkActor;
class vtkOctreePointLocator;
class vtkNamedColors;
class vtkColorTransferFunction;
class vtkBalloonWidget;

class Brain : public Animatable
{
    Q_OBJECT

public:
    // Constructor/Destructor
    Brain();
    ~Brain();
    // Getters/Setters
    Graph& getEEGGraph();
    Graph& getRegionsGraph();
    vtkSmartPointer<vtkActor> getActor();

    void setupBrainActor(QString brainPath, bool visibility, double opacity, vtkRenderer* renderer);
    void setupBrainRegionsGraph(QString resourcesDirPath, bool visibility, double opacity, vtkRenderer* renderer);
    void setupBalloonWidget(QVTKInteractor* interactor);
    void setupEEGNodes(QStandardItemModel* modelNodes, vtkRenderer* renderer);
    void setupEEGEdges(QList<QStandardItemModel*>& modelAlphaEdges, QList<QStandardItemModel*>& modelThetaEdges, vtkRenderer* renderer);
    void setNumOfFrames(const int lastFrame);
    void setInterval(const int interval);

    void resetEEGNodes();
    void resetVisibility(bool actor, bool nodes, bool regions);

    friend ostream& operator << (ostream& out, Brain& b)
    {
        out << "Actor: " << b.m_actor.GetPointer() << endl;
        out << b.m_graphEEG;
        return out;
    }

private:
    void invertNormals();
    void initShading();

private:
    Graph m_graphEEG;
    Graph m_graphRegions;
    QStandardItemModel *m_modelNodes;
    QList<QStandardItemModel*> m_modelAlphaEdges;
    QList<QStandardItemModel*> m_modelThetaEdges;
    QList<int> m_prevActReg;
    QList<int> m_prevNodeReg;
    QList<int> m_prevEdgeReg;
    QList<int> m_prevParReg;
    QTimer m_timer;
    QList<Particle*> m_particles;
    int m_particleSpeed;
    int m_particleFrame;
    int m_particlesLastFrame;
    int m_particlesUpdated;
    int m_timeWindowLastFrame;

    // VTK variables
    vtkSmartPointer<vtkActor> m_actor;
    vtkSmartPointer<vtkOctreePointLocator> m_octreeRegions;
    vtkSmartPointer<vtkOctreePointLocator> m_octreeEEG;
    vtkSmartPointer<vtkNamedColors> m_namedColors;
    vtkSmartPointer<vtkColorTransferFunction> m_colorTransferFunction;
    vtkSmartPointer<vtkBalloonWidget> m_balloonWidget;

public slots:
    void update(int frame) Q_DECL_OVERRIDE;
    void updateParticles();
    void checkParticles();

signals:
    void newParticleFrame(int frame);
    void readyForRendering();
    void endTimeWindowFrame();
};

#endif() /* _BRAIN_H */