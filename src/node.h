#ifndef _Node_H
#define _Node_H

#include <QString>

#include <vtkSmartPointer.h>    // Required for smart pointer internal ivars.
#include <vtkActor.h>
//class vtkActor;

class Node
{
public:
    // Constructor/Destructor
    Node();
    Node(QString label, const double* pos, vtkSmartPointer<vtkActor> actor);
    ~Node();
    // Getters/Setters
    const QString& getLabel() const;
    const double* getPos() const;
    vtkSmartPointer<vtkActor> getActor();
    const double* getActiveColor();

    void setLabel(const QString& label);
    void setPos(const double* pos);
    void setActor(vtkSmartPointer<vtkActor> actor);
    void setDefaultColor(const double* color);
    void setActiveColor(const double* color);

    void activate();
    void deactivate();

    friend ostream& operator << (ostream& out, Node &n)
    {
        return out << n.m_label.toStdString() << " -> [" << n.m_pos[0] << ", " << n.m_pos[1]
                   << ", " << n.m_pos[2] << "]" << " Actor: " << n.m_actor.GetPointer() << endl;
    }

private:
    QString m_label;
    double m_pos[3];
    double m_defaultColor[3];
    double m_activeColor[3];
    vtkSmartPointer<vtkActor> m_actor;
};

#endif /*_Node_H */