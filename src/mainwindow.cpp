﻿#include "ui_MainWindow.h"
#include "mainwindow.h"
#include "node.h"
#include "edge.h"
#include "graph.h"

#include <QFileDialog>
#include <QTableView>
#include <QDirIterator>
#include <QVector>
#include <QVector3D>
#include <QTimer>
#include <QDatetime>
#include <QDebug>

#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <QVTKInteractor.h>
#include <vtkErrorCode.h>
#include <vtkCamera.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkColorTransferFunction.h>
#include <vtkPolyData.h>
#include <vtkTransform.h>
#include <vtkDepthSortPolyData.h>
#include <vtkTimerLog.h>

#define VTK_CREATE(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

#define VTK_INIT(type, name) name = vtkSmartPointer<type>::New()

// Constructor
MainWindow::MainWindow() : m_model(0),
                           m_tableView(0),
                           m_animOn(false),
                           m_timeWindowframe(0),
                           m_lastFrame(0)
{
    setupUi(this);

    //-------------------------------WED QT/VTK-----------------------------------

    // VTK Renderer
    VTK_INIT(vtkRenderer, m_renderer);
    //m_renderer->SetBackground(0x33 / 255., 0x47 / 255., 0x34 / 255.);
    //m_renderer->SetBackground(255., 255., 255.);
    m_renderer->SetBackground(0, 0, 0);
    /*m_camera = m_renderer->GetActiveCamera();
    VTK_CREATE(vtkTransform, transform);
    transform->Identity();
    transform->RotateX(90);
    double camPos[3];
    m_camera->GetPosition(camPos);
    transform->TransformPoint(camPos, camPos);
    transform->Identity();
    transform->RotateZ(-90);
    transform->TransformPoint(camPos, camPos);
    m_camera->SetViewUp(0, 0, 1);
    m_camera->SetPosition(camPos);*/

    // QVTK Interactor
    m_interactor = m_qvtkWidget->GetInteractor();
    VTK_INIT(vtkInteractorStyleTrackballCamera, m_interactorStyle);
    m_interactor->SetInteractorStyle(m_interactorStyle);

    // VTK/Qt wedded
    m_renderWin = m_qvtkWidget->GetRenderWindow();
    m_renderWin->AddRenderer(m_renderer);

    //-------------------------------SET UP VTK-----------------------------------
    
    // Geometry
    m_brainModelPath = QApplication::applicationDirPath() + "/../../resources/objs/Full/brain.obj";
    m_regionResourcesDirPath = QApplication::applicationDirPath() + "/../../resources/objs/pial_DKT/";
    m_nodesPosPath = QApplication::applicationDirPath() + "/../../resources/positions/EEG_electrodes_coordinates.csv";
    m_matriciesDirPath = QApplication::applicationDirPath() + "/../../resources/matrices/Weighted/";

    // Balloon widget
    m_brain.setupBalloonWidget(m_interactor);

    // Brain
    m_brain.setupBrainActor(m_brainModelPath, m_checkBoxBrain->isChecked(), m_qwtSlider->value() / 100., m_renderer);
    setupGraph(m_nodesPosPath, m_matriciesDirPath);
    m_brain.setupBrainRegionsGraph(m_regionResourcesDirPath, m_checkBoxRegions->isChecked(), m_qwtSlider->value() / 100., m_renderer);

    //m_brain.resetEEGNodes();
    setupDepthPeeling();

    m_renderer->ResetCamera();
    m_interactor->Initialize();

    //-------------------------------VTK WIDGETS----------------------------------
    // Axes Widget
    VTK_CREATE(vtkAxesActor, axes);
    VTK_INIT(vtkOrientationMarkerWidget, m_orientationWidget);
    m_orientationWidget->SetOutlineColor(0.0, 0.0, 0.0);
    m_orientationWidget->SetOrientationMarker(axes);
    m_orientationWidget->SetInteractor(m_interactor);
    m_orientationWidget->SetViewport(0.0, 0.0, 0.12, 0.12);
    m_orientationWidget->SetEnabled(1);
    m_orientationWidget->InteractiveOff();

    //-------------------------------SET UP Qt------------------------------------
    // Set up action signals and slots
    connect(m_pushButtonPlayPause, &QPushButton::clicked, this, &MainWindow::onPlayPauseClicked);
    connect(m_sliderTimebar, &QSlider::sliderMoved, this, &MainWindow::onTimebarMoved);
    connect(m_checkBoxBrain, &QCheckBox::toggled, this, &MainWindow::onCheckBoxBrainToggled);
    connect(m_checkBoxRegions, &QCheckBox::toggled, this, &MainWindow::onCheckBoxRegionsToggled);
    connect(m_checkBoxNodes, &QCheckBox::toggled, this, &MainWindow::onCheckBoxNodesToggled);
    connect(m_qwtSlider, SIGNAL(valueChanged(double)), this, SLOT(onQwtSliderMoved(double)));
    connect(m_qwtWheel, SIGNAL(valueChanged(double)), this, SLOT(onValueChanged(double)));
    //connect(actionOpenFile, &QAction::triggered, this, &MainWindow::onOpenCSVFile);
    connect(actionOpenFile, &QAction::triggered, this, &MainWindow::onOpenTXTFile);
    connect(actionExit, &QAction::triggered, this, &MainWindow::onExit);
    // Animation control
    connect(this, &MainWindow::newTimeWindowFrame, &m_brain, &Brain::update);
    connect(&m_brain, &Brain::readyForRendering, this, &MainWindow::render);
    connect(&m_brain, &Brain::endTimeWindowFrame, this, &MainWindow::animate);
};

MainWindow::~MainWindow()
{
    // The smart pointers should clean up for vtk
    // The parent pointers passed to the constructors of QObjects should clean up before window destruction
    // TableView is a different window that we have to destroy
    if (m_tableView)
        delete m_tableView;
}

void MainWindow::setupGraph(QString nodesPosPath, QString matriciesDirPath)
{
    setupNodes(nodesPosPath);
    setupEdges(matriciesDirPath);
}

void MainWindow::setupNodes(QString nodesPosPath)
{
    QStandardItemModel* modelNodes = new QStandardItemModel(this);

    QFile file(nodesPosPath.toStdString().c_str());
    if (file.open(QIODevice::ReadOnly))
    {
        QList<QStandardItem*> standardItemList;
        QString data = file.readAll();
        data.remove(QRegularExpression("\r"));
        QString temp;
        QChar character;
        QTextStream textStream(&data);
        while (!textStream.atEnd()) {
            textStream >> character;
            if (character == ',' || character == '\n')
                checkString(modelNodes, standardItemList, temp, character);
            else if (textStream.atEnd())
            {
                temp.append(character);
                checkString(modelNodes, standardItemList, temp);
            }
            else temp.append(character);
        }
    }

    m_brain.setupEEGNodes(modelNodes, m_renderer);
}

void MainWindow::setupEdges(QString matriciesDirPath)
{
    QList<QStandardItemModel*> modelAlphaEdges;
    QList<QStandardItemModel*> modelThetaEdges;

    // Get Matricies from csv files
    QDirIterator it(matriciesDirPath, QStringList() << "*.csv",
                    QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext())
    {
        QString name = it.next();
        //qDebug() << name << endl;
        QFile file(name);
        if (file.open(QIODevice::ReadOnly)) {
            QStandardItemModel *modelEdges = new QStandardItemModel(this);
            QList<QStandardItem*> standardItemList;
            QString data = file.readAll();
            data.remove(QRegularExpression("\r"));
            QString temp;
            QChar character;
            QTextStream textStream(&data);
            while (!textStream.atEnd()) {
                textStream >> character;
                if (character == ',' || character == '\n')
                    checkString(modelEdges, standardItemList, temp, character);
                else if (textStream.atEnd())
                {
                    temp.append(character);
                    checkString(modelEdges, standardItemList, temp);
                }
                else temp.append(character);
            }
            
            if (name.contains(QString("alpha"), Qt::CaseInsensitive)) 
                modelAlphaEdges.append(modelEdges);
            if (name.contains(QString("theta"), Qt::CaseInsensitive))
                modelThetaEdges.append(modelEdges);
        }
    }

    m_lastFrame = modelAlphaEdges.count();
    m_sliderTimebar->setMaximum(m_lastFrame);

    m_brain.setupEEGEdges(modelAlphaEdges, modelThetaEdges, m_renderer);
    m_brain.setNumOfFrames(m_lastFrame);
}

void MainWindow::checkString(QStandardItemModel *model, QList<QStandardItem*> &standardItemList, QString &temp, QChar character)
{
    // if string has even number of " just append the char to the string
    // else construct Standard item and append it to the list if 
    if (temp.count("\"") % 2 == 0)
    {
        // replace "" with ' and remove those on the start and the end
        if (temp.startsWith(QChar('\"')) && temp.endsWith(QChar('\"')))
        {
            temp.remove(QRegularExpression("^\""));
            temp.remove(QRegularExpression("\"$"));
        }
        temp.replace("\"\"", "\"");

        QStandardItem *item = new QStandardItem(temp);
        standardItemList.append(item);
        if (character != QChar(','))
        {
            model->appendRow(standardItemList);
            standardItemList.clear();
        }
        temp.clear();
    }
    else temp.append(character);
}

void MainWindow::setupDepthPeeling()
{
    //*
    // Answer the key question: Does this box support GPU Depth Peeling?
    bool useDepthPeeling = isDepthPeelingSupported(false);
    //std::cout << "DEPTH PEELING SUPPORT: " << (useDepthPeeling ? "YES" : "NO") << std::endl;

    bool forceDepthSort = false;
    bool withoutAnyDepthThings = false;
    int success = false;

    // Use depth peeling if available and not explicitly prohibited, otherwise we
    // use manual depth sorting
    //cout << "CHOSEN MODE: ";
    if (useDepthPeeling && !forceDepthSort && !withoutAnyDepthThings) // GPU
    {
        int maxPeels = 100;
        double occulusionRatio = 0.5;
        // Setup GPU depth peeling with configured parameters
        success = setupEnvironmentForDepthPeeling(maxPeels, occulusionRatio);
        //cout << "*** DEPTH PEELING *** " << endl;
    }
    else if (!withoutAnyDepthThings) // CPU
    {
        cout << "*** DEPTH SORTING ***" << endl;
        // Setup CPU depth sorting filter
        VTK_CREATE(vtkDepthSortPolyData, depthSort);
        depthSort->SetInputConnection(m_brain.getActor()->GetMapper()->GetOutputPort());
        depthSort->SetDirectionToBackToFront();
        depthSort->SetVector(1, 1, 1);
        depthSort->SetCamera(m_camera);
        depthSort->SortScalarsOff();
        // do not really need this here
        // Bring it to the mapper's input
        m_brain.getActor()->GetMapper()->SetInputConnection(depthSort->GetOutputPort());
        depthSort->Update();
    }
    else
    {
        cout << "*** NEITHER DEPTH PEELING NOR DEPTH SORTING ***" << endl;
    }
    //*/
}

bool MainWindow::setupEnvironmentForDepthPeeling(int maxNoOfPeels, double occlusionRatio)
{
    if (!m_renderWin || !m_renderer)
        return false;

    // 1. Use a render window with alpha bits (as initial value is 0 (false)):
    m_renderWin->SetAlphaBitPlanes(true);

    // 2. Force to not pick a framebuffer with a multisample buffer
    // (as initial value is 8):
    m_renderWin->SetMultiSamples(0);

    // 3. Choose to use depth peeling (if supported) (initial value is 0 (false)):
    m_renderer->SetUseDepthPeeling(true);

    // 4. Set depth peeling parameters
    // - Set the maximum number of rendering passes (initial value is 4):
    m_renderer->SetMaximumNumberOfPeels(maxNoOfPeels);
    // - Set the occlusion ratio (initial value is 0.0, exact image):
    m_renderer->SetOcclusionRatio(occlusionRatio);

    return true;
}

bool MainWindow::isDepthPeelingSupported(bool doItOffScreen)
{
    if (!m_renderWin || !m_renderer)
        return false;

    bool success = true;

    // Save original renderer / render window state
    bool origOffScreenRendering = m_renderWin->GetOffScreenRendering() == 1;
    bool origAlphaBitPlanes = m_renderWin->GetAlphaBitPlanes() == 1;
    int origMultiSamples = m_renderWin->GetMultiSamples();
    bool origUseDepthPeeling = m_renderer->GetUseDepthPeeling() == 1;
    int origMaxPeels = m_renderer->GetMaximumNumberOfPeels();
    double origOcclusionRatio = m_renderer->GetOcclusionRatio();

    // Activate off screen rendering on demand
    m_renderWin->SetOffScreenRendering(doItOffScreen);

    // Setup environment for depth peeling (with some default parametrization)
    success = success && setupEnvironmentForDepthPeeling(100, 0.5);

    // Do a test render
    m_renderWin->Render();

    // Check whether depth peeling was used
    success = success && m_renderer->GetLastRenderingUsedDepthPeeling();

    // recover original state
    m_renderWin->SetOffScreenRendering(origOffScreenRendering);
    m_renderWin->SetAlphaBitPlanes(origAlphaBitPlanes);
    m_renderWin->SetMultiSamples(origMultiSamples);
    m_renderer->SetUseDepthPeeling(origUseDepthPeeling);
    m_renderer->SetMaximumNumberOfPeels(origMaxPeels);
    m_renderer->SetOcclusionRatio(origOcclusionRatio);

    return success;
}

void MainWindow::checkFrameRate()
{
    //*
    // Test frame rate
    // Check the average frame rate when rotating the actor
    int endCount = 100;
    VTK_CREATE(vtkTimerLog, clock);
    // Set a user transform for successively rotating the camera position
    VTK_CREATE(vtkTransform, transform);
    transform->Identity();
    transform->RotateY(2.0); // rotate 2 degrees around Y-axis at each iteration
    double camPos[3]; // camera position
    // Start test
    clock->StartTimer();
    for (int i = 0; i < endCount; i++)
    {
        m_camera->GetPosition(camPos);
        transform->TransformPoint(camPos, camPos);
        m_camera->SetPosition(camPos);
        m_renderWin->Render();
    }
    clock->StopTimer();
    double frameRate = (double)endCount / clock->GetElapsedTime();
    std::cout << "AVERAGE FRAME RATE: " << frameRate << " fps" << std::endl;
    //*/
}

// Slots
void MainWindow::onOpenCSVFile()
{
    if (!m_tableView ) m_tableView = new QTableView();
    if (!m_model) m_model = new QStandardItemModel(m_tableView);

    m_tableView->setModel(m_model);
    QString fileName = QFileDialog::getOpenFileName(this, "Open CSV file",
        QString(QCoreApplication::applicationDirPath() + "/../../resources/matrices/Weighted/Alpha/"), "CSV (*.csv)");

    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly))
    {
        QList<QStandardItem*> standardItemList;
        QString data = file.readAll();
        data.remove(QRegularExpression("\r"));
        QString temp;
        QChar character;
        QTextStream textStream(&data);
        while (!textStream.atEnd())
        {
            textStream >> character;
            if (character == ',' || character == '\n')
                checkString(m_model, standardItemList, temp, character);
            else if (textStream.atEnd())
            {
                temp.append(character);
                checkString(m_model, standardItemList, temp);
            }
            else temp.append(character);
        }
        m_tableView->show();
    }

    /*
    qDebug() << "Total rows:" << m_model->rowCount();
    qDebug() << "Total cols:" << m_model->columnCount();
    int rows = m_model->rowCount();
    int cols = m_model->columnCount();
    for (int i = 0; i < rows; i++)
    {
        cout << i << ": ";
        for (int j = 0; j < cols; j++)
        {
            cout  << " " << m_model->item(i, j)->text().toInt();
        }
        cout << endl;
    }
    //*/
}

void MainWindow::onOpenTXTFile()
{
    cout << "Reading txt!!" << endl;
    if (!m_tableView) m_tableView = new QTableView();
    if (!m_model) m_model = new QStandardItemModel(m_tableView);

    m_tableView->setModel(m_model);
    QString fileName = QFileDialog::getOpenFileName(this, "Open CSV file",
        QString(QCoreApplication::applicationDirPath() + "/../../resources/loreta/"), "TXT (*.txt)");

    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly))
    {
        cout << "File opened succ" << endl;
        QList<QStandardItem*> standardItemList;
        cout << __LINE__ << endl;
        QString data = file.readAll();
        cout << __LINE__ << endl;
        cout << "After read all" << endl;
        //data.remove(QRegularExpression("\r"));
        //data.remove(QChar('\r'));
        cout << __LINE__ << endl;
        QString temp;
        QChar character;
        QTextStream textStream(&data);
        int count = 0;
        cout << "B4 while MAX: " << std::numeric_limits<int>::max() << endl;
        while (!textStream.atEnd())
        {
            //cout << "+1 ";
            textStream >> character;
            if (character == ' ')
            {
                //checkString(m_model, standardItemList, temp, character);
                QStandardItem* item = new QStandardItem(temp);
                if (item) standardItemList.append(item);
                else cout << "No memory" << endl;
                temp.clear();
            }
            else if (character == '\n')
            {
                count++;
                m_model->appendRow(standardItemList);
                standardItemList.clear();
                if (count >= 10000)
                {
                    m_tableView->show();
                    return;
                }
            }
            else if (character != '\r')temp.append(character);
        }
        m_tableView->show();
    }

    //*
    qDebug() << "Total rows:" << m_model->rowCount();
    qDebug() << "Total cols:" << m_model->columnCount();
    int rows = m_model->rowCount();
    int cols = m_model->columnCount();
    for (int i = 0; i < rows; i++)
    {
        cout << i << ": ";
        for (int j = 0; j < cols; j++)
        {
            cout  << " " << m_model->item(i, j)->text().toInt();
        }
        cout << endl;
    }
    //*/
}

void MainWindow::onExit() {
    qApp->exit();
}

void MainWindow::onPlayPauseClicked()
{
    // Pause
    if (m_animOn)
    {
        //qDebug() << "Pause clicked";
        m_pushButtonPlayPause->setText("Play");
        m_pushButtonPlayPause->setIcon(QIcon(":/Icons/play.png"));
        disconnect(&m_brain, &Brain::endTimeWindowFrame, this, &MainWindow::animate);
    }
    // Play
    else
    {
        //qDebug() << "Play clicked";
        m_pushButtonPlayPause->setText("Pause");
        m_pushButtonPlayPause->setIcon(QIcon(":/Icons/pause.png"));
        connect(&m_brain, &Brain::endTimeWindowFrame, this, &MainWindow::animate);
        animate();
    }

    m_animOn = !m_animOn;
}

void MainWindow::onTimebarMoved(int val)
{
    if (m_animOn) return;
    m_timeWindowframe = val;

    // Update edges
    animate();
}

void MainWindow::onValueChanged(double val)
{
    //qDebug() << "Wheel changed value to " << val;

    m_labelSpeedValue->setText(QString::number(m_qwtWheel->value(), 'f', 2));
}

void MainWindow::onQwtSliderMoved(double val)
{
    m_brain.getActor()->GetProperty()->SetOpacity(val / 100.);

    for (int i = 0; i < m_brain.getRegionsGraph().getNodes().size(); i++)
    {
        m_brain.getRegionsGraph().getNode(i).getActor()->GetProperty()->SetOpacity(val / 100.);
    }
    // Call update so that changes will be rendered
    m_qvtkWidget->update();
}

void MainWindow::onCheckBoxBrainToggled(bool checked)
{
    m_brain.getActor()->SetVisibility(checked);

    // Call update so that changes will be rendered
    m_qvtkWidget->update();
}

void MainWindow::onCheckBoxRegionsToggled(bool checked)
{
    QList<Node>& regions = m_brain.getRegionsGraph().getNodes();
    int numr = regions.size();
    for (int i = 0; i < numr; i++)
    {
        regions[i].getActor()->SetVisibility(checked);
    }

    // Call update so that changes will be rendered
    m_qvtkWidget->update();
}

void MainWindow::onCheckBoxNodesToggled(bool checked)
{
    QList<Node>& nodes = m_brain.getEEGGraph().getNodes();
    int numn = nodes.size();
    for (int i = 0; i < numn; i++)
    {
        nodes[i].getActor()->SetVisibility(checked);
    }

    // Call update so that changes will be rendered
    m_qvtkWidget->update();
}

void MainWindow::animate()
{
    // Stop animation or repeat depending on checkbox value
    if (m_timeWindowframe == m_lastFrame)
    {
        m_timeWindowframe = 0;
        // Stop
        if (!m_checkboxRepeat->isChecked())
        {
            m_animOn = false;
            m_sliderTimebar->setValue(m_sliderTimebar->maximum());
            m_pushButtonPlayPause->setText("Play");
            m_pushButtonPlayPause->setIcon(QIcon(":/Icons/play.png"));
            m_sliderTimebar->setValue(m_timeWindowframe);

            m_brain.resetVisibility(m_checkBoxBrain->isChecked(), m_checkBoxNodes->isChecked(), m_checkBoxRegions->isChecked());
            disconnect(&m_brain, &Brain::endTimeWindowFrame, this, &MainWindow::animate);

            // Call update so that changes will be rendered
            m_qvtkWidget->update();
            return;
        }
    }

    // Update edges/particles
    emit newTimeWindowFrame(m_timeWindowframe);

    // Update timebar
    m_sliderTimebar->setValue(m_timeWindowframe);

    m_timeWindowframe++;

}

void MainWindow::render()
{
    //qDebug() << "Time elapsed" << m_timeTester.elapsed();
    //m_framesUpdated++;
    //if (m_framesUpdated == m_lastFrame)
    // Call update so that changes will be rendered
    m_qvtkWidget->update();
}