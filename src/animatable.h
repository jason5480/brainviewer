#ifndef _ANIMATABLE_H
#define _ANIMATABLE_H

#include <QObject>

class Animatable : public QObject
{
    Q_OBJECT

public:
    explicit Animatable() : QObject(NULL){}

public slots:
    virtual void update(int frame) = 0;
};

#endif