#ifndef _Edge_H
#define _Edge_H

#include "node.h"
#include <vtkSmartPointer.h>    // Required for smart pointer internal ivars.

class QVector3D;

class vtkActor;

class Edge
{
public:
    // Constructor/Destructor
    Edge(Node& from, int fromID, Node& to, int toID, vtkSmartPointer<vtkActor> actor);
    ~Edge();
    // Getters/Setters
    Node& getFrom();
    int getFromID();
    Node& getTo();
    int getToID();
    vtkSmartPointer<vtkActor> getActor();
    void setFrom(Node& node);
    void setTo(Node& node);

    void update();
    //*
    friend ostream& operator << (ostream& out, Edge &e)
    {
        return out << "From: " << e.getFrom().getLabel().toStdString()
                   << " -> To: " << e.getTo().getLabel().toStdString()
                   << " Actor: " << e.getActor().GetPointer() << endl;
    }
    //*/
private:
    Node& m_from;
    int m_fromID;
    Node& m_to;
    int m_toID;
    vtkSmartPointer<vtkActor> m_actor;
};

#endif /* _Edge_H */