#ifndef _MainWindow_H
#define _MainWindow_H

#include "ui_mainwindow.h"
#include "brain.h"
//#include "graph.h"

#include <QMainWindow>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QStandardItemmodel>
#include <QTimer>

#include <vtkSmartPointer.h>    // Required for smart pointer internal ivars.

// Forward Qt class declarations
class QString;
class QVTKInteractor;
class QTableView;

// Forward VTK class declarations
class vtkPolyDataMapper;
class vtkRender;
class vtkRenderWindow;
class vtkInteractorStyleTrackballCamera;
class vtkCamera;
class vtkOrientationMarkerWidget;
class vtkBalloonWidget;
class vtkNamedColors;
class vtkColorTransferFunction;

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    // Constructor/Destructor
    MainWindow();
    ~MainWindow();

private:
    // Brain
    Brain m_brain;

    // Qt variables
    QString m_brainModelPath;
    QString m_regionResourcesDirPath;
    QString m_nodesPosPath;
    QString m_matriciesDirPath;
    QStandardItemModel* m_model;
    QTableView* m_tableView;

    // VTK variables
    vtkSmartPointer<vtkRenderer> m_renderer;
    vtkSmartPointer<vtkRenderWindow> m_renderWin;
    vtkSmartPointer<vtkInteractorStyleTrackballCamera> m_interactorStyle;
    vtkSmartPointer<QVTKInteractor> m_interactor;
    vtkSmartPointer<vtkCamera> m_camera;
    vtkSmartPointer<vtkOrientationMarkerWidget> m_orientationWidget;

    // Animation
    bool m_animOn;
    int m_timeWindowframe;
    int m_lastFrame;

private:
    // Auxiliary methods
    void setupGraph(QString nodesPosPath, QString matriciesDirPath);
    void setupNodes(QString nodesPosPath);
    void setupEdges(QString matriciesDirPath);
    void checkString(QStandardItemModel *model, QList<QStandardItem*> &standardItemList, QString &temp, QChar character = 0);
    void setupDepthPeeling();
    bool setupEnvironmentForDepthPeeling(int maxNoOfPeels, double occlusionRatio);
    bool isDepthPeelingSupported(bool doItOffScreen);
    void checkFrameRate();

private slots :
    void onOpenCSVFile();
    void onOpenTXTFile();
    void onExit();
    void onPlayPauseClicked();
    void onTimebarMoved(int val);
    void onValueChanged(double val);
    void onQwtSliderMoved(double val);
    void onCheckBoxBrainToggled(bool checked);
    void onCheckBoxRegionsToggled(bool checked);
    void onCheckBoxNodesToggled(bool checked);
    void animate();
    void render();

signals:
    void newTimeWindowFrame(int frame);
};

#endif /* _MainWindow_H */
