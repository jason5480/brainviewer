#ifndef _Graph_H
#define _Graph_H

#include "node.h"
#include "edge.h"

#include <QList>

class Graph
{
public:
    // Constructor/Destructor
    Graph();
    ~Graph();
    // Getters/Setters
    QList<Node>& getNodes();
    Node& getNode(int i);
    QList<Edge>& getEdges();
    Edge& getEdge(int i);
    void setNode(int i, const double* pos, vtkSmartPointer<vtkActor> actor);
    void createNode(QString label, const double* pos, vtkSmartPointer<vtkActor> actor);
    void createEdge(Node& from, int fromID, Node& to, int toID, vtkSmartPointer<vtkActor> actor);
    void Graph::addNode(Node& node);
    void Graph::addEdge(Edge& edge);

    //*
    friend ostream& operator << (ostream& out, Graph& g)
    {
        int numn = g.getNodes().size();
        out << "-----NODES-----: " << numn << endl;
        for (int i = 0; i < numn; i++)
        {
            out << g.getNode(i);
        }
        int nume = g.getEdges().size();
        out  << endl << "-----EDGES-----: " << nume << endl;
        for (int i = 0; i < nume; i++)
        {
            Edge e = g.getEdge(i);
            if (e.getActor()->GetVisibility()) out << e;
        }
        return out;
    }
    //*/
private:
    QList<Node> m_nodes;
    QList<Edge> m_edges;
};

#endif /* _Graph_H */